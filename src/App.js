import "./App.css";
import {useState} from 'react';

function App() {
const [text, setText]=useState(()=>{
  
  if (localStorage.getItem('text')!=null) {
   console.log('inside Ls',localStorage.getItem('text'));
    return localStorage.getItem('text')
  } else {
    console.log('first start and LS is: ',localStorage.getItem('text'));
    return '';
  }

});
 
console.log(text);
let valueTextAr=typeof text=='string'? text :  text.text 

  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
           
            <textarea onChange={(e)=>setText({text:e.target.value})} value={valueTextAr}  className="textarea is-large" placeholder="Notes..." />
          </div>
        </div>
        <button onClick={(e)=>{localStorage.setItem('text',text.text)}} className="button is-large is-primary is-active">Save</button>
        <button onClick={(e)=>setText({text:''})} className="button is-large">Clear</button>
      </div>
    </div>
  );
}

export default App;
